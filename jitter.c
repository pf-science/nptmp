#include <stdio.h>
#include <math.h>

#define GAMMA 0.577215664901532860606512090082402431042

double avg(unsigned long long _x)
{
	return log(_x) + (2 * GAMMA - 1);
}

int main(int argc, char **argv)
{
	(void) argc;
	(void) argv;
	FILE *fin = fopen("sum.seq", "r");
	unsigned long long x = 0;
	double y, sum = 0;
	while (!feof(fin))
	{
		fscanf(fin, "%llu\t%lf\n", &x, &y);
		sum += pow(y - avg(x), 2);
	}
	fclose(fin);
	double sigma = sqrt(sum / x);
	printf("Summary error:\t%1.16lf\n", sum);
	printf("Sigma:\t\t%1.16lf\n", sigma);
	return 0;
}

