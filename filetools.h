#include <sys/types.h>

off_t fsize(const char *filename);
char *fgetlastline(FILE *arg, off_t filesize);

