DESTDIR?=
PREFIX?=/usr/local
CC=gcc
NPTMP_CFLAGS=-march=native -mtune=native -Ofast -std=c99 -lpthread -W -Wall -pedantic -march=native -mtune=native -fopenmp -lm -D_FILE_OFFSET_BITS=64
ANALYZE_CFLAGS=-Ofast -std=c99 -lm -W -Wall -pedantic -D_FILE_OFFSET_BITS=64
JITTER_CFLAGS=-Ofast -std=c99 -lm -W -Wall -pedantic -D_FILE_OFFSET_BITS=64
FIRST_CFLAGS=-Ofast -std=c99 -lm -W -Wall -pedantic -D_FILE_OFFSET_BITS=64
all: nptmp analyze jitter first
nptmp: nptmp.o filetools.o
	$(CC) $(NPTMP_CFLAGS) -lc nptmp.o filetools.o -o $@
nptmp.o: nptmp.c
	$(CC) $(NPTMP_CFLAGS) -c nptmp.c -o $@
analyze: analyze.o filetools.o
	$(CC) $(ANALYZE_CFLAGS) -lc analyze.o filetools.o -o $@
analyze.o: analyze.c
	$(CC) $(ANALYZE_CFLAGS) -c analyze.c -o $@
filetools.o: filetools.c
	$(CC) $(ANALYZE_CFLAGS) -c filetools.c -o $@
jitter: jitter.o
	$(CC) $(JITTER_CFLAGS) -lc jitter.o -o $@
jitter.o: jitter.c
	$(CC) $(JITTER_CFLAGS) -c jitter.c -o $@
first: first.o
	$(CC) $(FIRST_CFLAGS) -lc first.o -o $@
first.o: first.c
	$(CC) $(FIRST_CFLAGS) -c first.c -o $@
install:
	install -Dm 0755 nptmp $(DESTDIR)$(PREFIX)/bin/nptmo
	install -Dm 0755 analyze $(DESTDIR)$(PREFIX)/bin/analyze
	install -Dm 0755 jitter $(DESTDIR)$(PREFIX)/bin/jitter
	install -Dm 0755 first $(DESTDIR)$(PREFIX)/bin/first
clean:
	rm -f nptmp.o
	rm -f nptmp
	rm -f analyze.o
	rm -f analyze
	rm -f jitter.o
	rm -f jitter
	rm -f filetools.o
	rm -f first.o
	rm -f first
