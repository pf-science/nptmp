#include <limits.h>
#include <math.h>
#include <omp.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <sys/time.h>
#include "filetools.h"
#include "nptmp_errors.h"

static volatile int do_exit = 0;

void sigterm_handler(int signum)
{
	printf("Caught %d signal. Performing graceful shutdown...\n", signum);
	if (!do_exit)
		do_exit = 1;
}

uint64_t func(uint64_t n)
{
	uint64_t count = 1;
	uint64_t sqr = (uint64_t)sqrt(n);
	for (uint64_t i = 2; i <= sqr; i = (i == 2 ? 3 : i + 2))
	{
		uint64_t pow = 0;
		while (n % i == 0)
		{
			pow++;
			n /= i;
		}
		if (pow != 0)
		{
			count *= pow + 1;
			sqr = (uint64_t)sqrt(n);
		}
	}
	if (n != 1)
		count *= 1 + 1;
	return count;
}

int main(int argc, char **argv)
{
	(void) argc;
	(void) argv;

	//install SIGTERM/SIGINT handler
	signal(SIGTERM, sigterm_handler);
	signal(SIGINT, sigterm_handler);

	//shows limits
	printf("Calculation can be done up to f(%llu)\n", ULLONG_MAX);

	//detect CPUs count
	int cpus = omp_get_max_threads();
	printf("Using %d thread(s)\n", cpus);

	//be idle
	printf("Using idle priority\n");
	setpriority(PRIO_PROCESS, 0, 19);

	uint64_t current = 1;

	//determine restore point
	FILE *f_seq = fopen("npt.seq", "r");
	if (f_seq == NULL)
	{
		printf("File doesn't seem to exist, creating it. Assuming clean start.\n");
	} else
	{
		printf("Checking restore point...\n");
		off_t fs = fsize("npt.seq");
		if (fs < 0)
		{
			fprintf(stderr, "Cannot get file size!\n");
			exit(EFILESIZE);
		}
		char *last_line = fgetlastline(f_seq, fs);
		if (last_line == NULL)
		{
			fprintf(stderr, "Cannot get last line!\n");
			exit(ELASTLINE);
		}

		fclose(f_seq);

		uint64_t last_x = 0, last_y = 0;
		int ret = sscanf(last_line, "%ju\t%ju\n", &last_x, &last_y);
		if (ret < 2)
		{
			fprintf(stderr, "Cannot parse last line!\n");
			exit(EPARSELASTLINE);
		}
		printf("Checkpoint is: f(%ju)=%ju\n", last_x, last_y);
		current = last_x + 1;
	}

	//output file
	f_seq = fopen("npt.seq", "a");
	if (f_seq == NULL)
	{
		fprintf(stderr, "Cannot write to file!\n");
		exit(EFILEWRITE);
	}

	//main loop
	uint64_t thousand = 0, cycle_count = 0;
        struct timeval time_start;
	gettimeofday(&time_start, NULL);
	for (uint64_t i = current; i < ULLONG_MAX; i += cpus)
	{
		if (!do_exit)
		{
			uint64_t y[cpus];
			
			#pragma omp parallel for
			for (uint64_t j = i; j < i + cpus; j++)
				y[j - i] = func(j);
			
			for (uint64_t j = i; j < i + cpus; j++)
				fprintf(f_seq, "%ju\t%ju\n", j, y[j - i]);
			
			thousand += cpus;
			cycle_count += cpus;
			if (thousand >= 1000)
			{
				struct timeval time_end;
				gettimeofday(&time_end, NULL);
				double cycle_time = (time_end.tv_sec + time_end.tv_usec / 1000000.0) - (time_start.tv_sec + time_start.tv_usec / 1000000.0);
				if (cycle_time >= 1.0)
				{
					double speed = cycle_count / cycle_time;
					printf("Done:\n");
					printf("\ttotal: %ju\n", i + cpus - 1);
					printf("\tthis cycle: %ju\n", cycle_count);
					printf("took %1.3lf s, speed: %1.3lf nums/sec\n", cycle_time, speed);
					gettimeofday(&time_start, NULL);
					cycle_count = 0;
					fflush(f_seq);
				}
				thousand = 0;
			}
		} else
			break;
	}
	
	printf("Closing file...\n");
	fclose(f_seq);
	
	printf("Done.\n");
	return 0;
}

