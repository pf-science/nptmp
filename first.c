#include <stdio.h>
#include <stdlib.h>
#include "analyze.h"

int main(int argc, char **argv)
{
	(void) argc;
	(void) argv;

	FILE *f_seq = fopen("npt.seq", "r");

	unsigned long long distribution[DISTR_LENGTH];
	for (unsigned long long i = 0; i < DISTR_LENGTH; i++)
		distribution[i] = 0;

	while (!feof(f_seq))
	{
		unsigned long long x, y;
		fscanf(f_seq, "%llu\t%llu\n", &x, &y);

		if (y < DISTR_LENGTH)
		{
			if (distribution[y] == 0)
				distribution[y] = x;
		}
		else
			fprintf(stderr, "Divisors count is out of range: f(%llu)=%llu\n", x, y);
	}

	fclose(f_seq);

	FILE *f_distr = fopen("first.out", "w");
	for (unsigned long long i = 0; i < DISTR_LENGTH; i++)
		if (distribution[i] != 0)
			fprintf(f_distr, "%llu\t%llu\n", i, distribution[i]);
	fclose(f_distr);

	return 0;
}

