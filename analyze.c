#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "analyze.h"
#include "filetools.h"

int main(int argc, char **argv)
{
	(void) argc;
	(void) argv;

	FILE *f_seq = fopen("npt.seq", "r");
	FILE *sum_seq = fopen("sum.seq", "w");

	unsigned long long sum = 0, count = 0, prev_x = 0, prev_y = 0, sum_step = 0, max_x = 0, max_y = 0,
		      max_square_x = 0, max_square_y = 0, squares = 0, primes = 0, max_prime_x = 0,
		      adj_seq = 1, adj_seq_x = 0, adj_seq_y = 0, adj_seq_max = 0, adj_seq_max_x = 0,
		      adj_seq_max_y = 0;
	unsigned long long distribution[DISTR_LENGTH];
	for (unsigned long long i = 0; i < DISTR_LENGTH; i++)
		distribution[i] = 0;

	while (!feof(f_seq))
	{
		unsigned long long x, y;
		fscanf(f_seq, "%llu\t%llu\n", &x, &y);

		if (y == prev_y)
		{
			adj_seq++;
			adj_seq_x = x;
			adj_seq_y = y;
		} else
		{
			if (adj_seq >= adj_seq_max)
			{
				adj_seq_max = adj_seq;
				adj_seq_max_x = adj_seq_x;
				adj_seq_max_y = adj_seq_y;
			}
			adj_seq = 1;
		}

		if (y < DISTR_LENGTH)
			distribution[y]++;
		else
			fprintf(stderr, "Divisors count is out of range: f(%llu)=%llu\n", x, y);

		if (y > max_y)
		{
			max_y = y;
			max_x = x;
		}

		if (y % 2 != 0)
		{
			squares++;
			if (x > max_square_x)
			{
				max_square_x = x;
				max_square_y = y;
			}
		}

		if (y == 2)
		{
			primes++;
			if (x > max_prime_x)
				max_prime_x = x;
		}

		if (sum + y < sum)
		{
			fprintf(stderr, "Sum overflow detected at %llu. Aborting.\n", x);
			fclose(f_seq);
			fclose(sum_seq);
			exit(2);
		} else
			sum += y;
		sum_step += y - prev_y;
		prev_y = y;
		if (x - prev_x != 1)
		{
			printf("Error in sequence at %llu!\n", prev_x);
			fclose(f_seq);
			fclose(sum_seq);
			exit(1);
		} else
			prev_x = x;

		fprintf(sum_seq, "%llu\t%1.16lf\n", x, (double)sum / x);

		count++;
	}

	off_t fs = fsize("npt.seq");
	char *lastline = fgetlastline(f_seq, fs);

	fclose(sum_seq);
	fclose(f_seq);

	double avg = (double)sum / count;
	double avg_step = (double)sum_step / count;
	unsigned long long last_x = 0, last_y = 0;
	sscanf(lastline, "%llu\t%llu\n", &last_x, &last_y);
	
	printf("fsize:\t\t\t%llu\n", (unsigned long long)fs);
	printf("Last:\t\t\tf(%llu)=%llu\n", last_x, last_y);
	printf("Max:\t\t\tf(%llu)=%llu\n", max_x, max_y);
	printf("Avg.:\t\t\t%1.16lf\n", avg);
	printf("Avg. step:\t\t%1.16lf\n", avg_step);
	printf("Squares:\t\t%llu\n", squares);
	printf("Max. square:\t\tf(%llu)=%llu\n", max_square_x, max_square_y);
	printf("Primes:\t\t\t%llu\n", primes);
	printf("Max. prime:\t\t%llu\n", max_prime_x);
	printf("Longest sequence:\tf(%llu)=%llu, n=%llu\n", adj_seq_max_x, adj_seq_max_y, adj_seq_max);

	unsigned long long distr_sum = 0;
	for (unsigned long long i = 0; i < DISTR_LENGTH; i++)
		distr_sum += distribution[i];

	double distribution_n[DISTR_LENGTH];
	for (unsigned long long i = 0; i < DISTR_LENGTH; i++)
		distribution_n[i] = (double)distribution[i] / distr_sum;

	FILE *f_distr = fopen("distr.out", "w");
	for (unsigned long long i = 0; i < DISTR_LENGTH; i++)
		if (distribution_n[i] != 0)
			fprintf(f_distr, "%llu\t%1.16lf\n", i, distribution_n[i]);
	fclose(f_distr);

	return 0;
}

