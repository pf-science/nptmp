#include <math.h>
#include <omp.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static uint64_t func(uint64_t n)
{
	uint64_t count = 1;
	uint64_t sqr = (uint64_t)sqrt(n);
	for (uint64_t i = 2; i <= sqr; i = (i == 2 ? 3 : i + 2))
	{
		uint64_t pow = 0;
		while (n % i == 0)
		{
			pow++;
			n /= i;
		}
		if (pow != 0)
		{
			count *= pow + 1;
			sqr = (uint64_t)sqrt(n);
		}
	}
	if (n != 1)
		count *= 1 + 1;
	return count;
}

int main(int argc, char **argv)
{
	(void) argc;
	(void) argv;

	uint64_t x0 = 0;
	uint64_t y0 = 0;
	uint64_t seq_len = 1;
	uint64_t seq_len_max = 1;
	uint64_t seq_x = UINT64_MAX;

#pragma omp parallel for shared(x0,y0,seq_len,seq_len_max,seq_x)
	for (uint64_t x = 1; x < UINT64_MAX; x++)
	{
		uint64_t y = func(x);
		if (y == y0)
		{
			seq_len++;
			if (seq_x == UINT64_MAX)
				seq_x = x0;
		} else
		{
			if (seq_len > seq_len_max)
			{
				printf("f(%zu .. %zu [%zu])=%zu\n", seq_x, seq_x + seq_len - 1, seq_len, y0);
				seq_len_max = seq_len;
			}
			seq_len = 1;
			seq_x = UINT64_MAX;
		}
		y0 = y;
		x0 = x;
	}

	return 0;
}

