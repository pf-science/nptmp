#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "filetools.h"

off_t fsize(const char *filename)
{
	struct stat st; 
	if (stat(filename, &st) == 0)
		return st.st_size;
	return -1; 
}

char *fgetlastline(FILE *arg, off_t filesize)
{
	char *line = NULL;
	fseek(arg, 0, SEEK_END);
	off_t index = -1;
	while (index > -filesize)
	{
		fseek(arg, --index, SEEK_END);
		int c = fgetc(arg);
		if (c == '\r' || c == '\n')
		{
			line = malloc(abs(index) * sizeof(char));
			if (line != NULL)
				fgets(line, abs(index), arg);
			break;
		}
	}
	return line;
}

